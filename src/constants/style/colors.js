const COLORS = {
  white: 'white',
  black: 'black',
  primary: '#fdf18d',
  primaryHover: '#f8e865',
  secondary: '#fa826f',
  secondaryHover: '#ff8d7e',
  success: '#70cd9a',
  dark: '#26303c',
  darkHover: '#182029',
  light: '#f0f3f7',
  lightHover: '#e9ecf0',
  whiteHover: '#f0f3f7',
  overlay: 'rgba(33, 47, 62, 0.7)',
  background: '#f9f9f9',
  backgroundLight: '#f4f5f8',
  backgroundDark: '#dadfe5',
};

export default COLORS;
