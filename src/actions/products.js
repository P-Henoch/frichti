import { API, HEADERS } from '../api';
import routes from '../api/routes.json';

import {
  GET_PRODUCTS,
  SET_PRODUCTS_IN_CART,
} from '../constants/products';

const productsService = routes.products;

export const getProducts = () => (dispatch) => {
  API.get(productsService.getProducts.url, HEADERS)
    .then((res) => {
      dispatch({
        type: `${GET_PRODUCTS}_SUCCESS`,
        payload: res.data.response[0].items,
      });
    })
    .catch(err => dispatch({
      type: `${GET_PRODUCTS}_REJECTED`,
      payload: err,
    }));
};

export const addProductsInCart = products => ({
  type: SET_PRODUCTS_IN_CART,
  payload: products,
});
