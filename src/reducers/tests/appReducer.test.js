// Reducer to be tested
import appReducer, { initialState } from '../appReducer';
import { GET_PRODUCTS, SET_PRODUCTS_IN_CART } from '../../constants/products';

describe('appReducer', () => {
  describe('INITIAL_STATE', () => {
    test('is correct', () => {
      const action = { type: 'test' };
      expect(appReducer(undefined, action)).toEqual(initialState);
    });
  });
  describe(`${GET_PRODUCTS}_SUCCESS`, () => {
    test('returns the correct state', () => {
      const product = { description: 'La soupe aux choux' };
      const products = [{ product }];
      const action = {
        type: `${GET_PRODUCTS}_SUCCESS`,
        payload: products,
      };
      const expectedState = {
        ...initialState,
        products: [product],
      };
      expect(appReducer(undefined, action)).toEqual(expectedState);
    });
  });
  describe(`${GET_PRODUCTS}_REJECTED`, () => {
    test('returns the correct state', () => {
      const action = {
        type: `${GET_PRODUCTS}_REJECTED`,
        payload: 'err',
      };
      const expectedState = {
        ...initialState,
        error: action.payload,
        isFetching: false,
      };
      expect(appReducer(undefined, action)).toEqual(expectedState);
    });
  });
  describe(`${GET_PRODUCTS}_PENDING`, () => {
    test('returns the correct state', () => {
      const action = {
        type: `${GET_PRODUCTS}_PENDING`,
      };
      const expectedState = {
        ...initialState,
        error: null,
        isFetching: true,
      };
      expect(appReducer(undefined, action)).toEqual(expectedState);
    });
  });
  describe(`${SET_PRODUCTS_IN_CART}`, () => {
    test('returns the correct state', () => {
      const products = [{ description: 'La soupe aux choux' }];
      const action = {
        type: `${SET_PRODUCTS_IN_CART}`,
        payload: products,
      };
      const expectedState = {
        ...initialState,
        productsInCart: action.payload,
      };
      expect(appReducer(undefined, action)).toEqual(expectedState);
    });
  });
});
