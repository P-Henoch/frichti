import { GET_PRODUCTS, SET_PRODUCTS_IN_CART } from '../constants/products';

export const initialState = {
  error: null,
  isFetching: false,
  products: null,
  productsInCart: null,
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case `${GET_PRODUCTS}_SUCCESS`:
      return {
        ...state,
        error: null,
        isFetching: false,
        products: action.payload.map(item => item.product),
      };
    case `${GET_PRODUCTS}_REJECTED`:
      return {
        ...state,
        error: action.payload,
        isFetching: false,
      };
    case `${GET_PRODUCTS}_PENDING`:
      return {
        ...state,
        error: null,
        isFetching: true,
      };
    case `${SET_PRODUCTS_IN_CART}`:
      return {
        ...state,
        productsInCart: action.payload,
      };
    default:
      return state;
  }
}
