## Tools/Libraries
Libraries I added:

- [axios](https://github.com/axios/axios): Make http request
- [prop-types](https://github.com/facebook/prop-types): Type check props
- [react-redux](https://github.com/reduxjs/react-redux): React bindings for redux
- [redux](https://github.com/reduxjs/redux): Manage application state
- [redux-thunk](https://github.com/reduxjs/redux-thunk): Write async logic for the store
- [styled-components](https://github.com/styled-components/styled-components): Styling

## Restitution

- [BitBucket](https://bitbucket.org/P-Henoch/frichti/src/master/)
- [http://ugly-thumb.surge.sh/](http://ugly-thumb.surge.sh/)
