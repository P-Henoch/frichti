import axios from 'axios';

export const API = axios.create({
  baseURL: 'https://api.myjson.com',
});

export const HEADERS = {
  headers: {
    'Content-Type': 'application/json',
  },
};
