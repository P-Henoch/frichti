import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import './App.css';
import MenuPage from '../../components/_pages/MenuPage/menu-page.connect';
import CartPage from '../../components/_pages/CartPage/cart-page.connect';

function App() {
  return (
    <Router>
      <div>
        <Route exact path="/" component={MenuPage} />
        <Route path="/panier" component={CartPage} />
      </div>
    </Router>
  );
}

export default App;
