import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';

const configureStore = createStore(
  reducers,
  compose(
    applyMiddleware(thunk),
  ),
);

export default configureStore;
