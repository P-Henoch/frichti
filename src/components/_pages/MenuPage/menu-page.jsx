import React from 'react';
import PropTypes from 'prop-types';

import ProductList from '../../_organisms/ProductsList/products-list';
import Header from '../../_molecules/Header/header';
import Cart from '../../../assets/images/cart.svg';

import StyledMenuPage from './menu-page.style';

class MenuPage extends React.Component {
  componentDidMount() {
    const { getProducts } = this.props;
    getProducts();
  }

  render() {
    const {
      products,
      addProductsInCart,
      productsInCart,
      isFetching,
    } = this.props;
    let productsQuantity = 0;
    productsQuantity = productsInCart && productsInCart.map(
      productInCart => productInCart.quantity,
    ).reduce((a, b) => a + b);
    let routeTitle = '';
    if (productsQuantity > 0) routeTitle = `(${productsQuantity})`;
    return (
      <StyledMenuPage>
        <Header title="Nos produits" route="/panier" routeTitle={routeTitle} img={Cart} />
        {isFetching ? (
          <div>Loading...</div>
        ) : (
          <ProductList
            products={products}
            productsInCart={productsInCart}
            addProductsInCart={addProductsInCart}
          />
        )}
      </StyledMenuPage>
    );
  }
}

MenuPage.propTypes = {
  products: PropTypes.array,
  productsInCart: PropTypes.array,
  getProducts: PropTypes.func,
  addProductsInCart: PropTypes.func,
  isFetching: PropTypes.bool,
};

MenuPage.defaultProps = {
  products: null,
  productsInCart: null,
  getProducts: () => {},
  addProductsInCart: () => {},
  isFetching: false,
};

export default MenuPage;
