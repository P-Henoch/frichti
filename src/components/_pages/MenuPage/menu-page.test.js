import React from 'react';
import { shallow } from 'enzyme';

import StyledMenuPage from './menu-page.style';

describe('<MenuPage />', () => {
  test('render', () => {
    const wrapper = shallow(
      <StyledMenuPage />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
