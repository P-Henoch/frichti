import styled from 'styled-components';
import COLORS from '../../../constants/style/colors';

const MenuPage = styled.div`
  background: ${COLORS.white};
  width: 100%;
  color: #000;
`;

export default MenuPage;
