import { connect } from 'react-redux';
import MenuPage from './menu-page';
import {
  getProducts,
  addProductsInCart,
} from '../../../actions/products';

const mapStateToProps = state => ({
  products: state.app.products,
  productsInCart: state.app.productsInCart,
  isFetching: state.app.isFetching,
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getProducts: () => dispatch(getProducts()),
    addProductsInCart: product => dispatch(addProductsInCart(product)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MenuPage);
