import { connect } from 'react-redux';
import CartPage from './cart-page';
import {
  getProducts,
  addProductsInCart,
} from '../../../actions/products';

const mapStateToProps = state => ({
  productsInCart: state.app.productsInCart,
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getProducts: () => dispatch(getProducts()),
    addProductsInCart: product => dispatch(addProductsInCart(product)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CartPage);