import React from 'react';
import { shallow } from 'enzyme';

import CartPage from './cart-page';

describe('<CartPage />', () => {
  test('render', () => {
    const wrapper = shallow(
      <CartPage />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
