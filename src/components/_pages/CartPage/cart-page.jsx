import React from 'react';
import PropTypes from 'prop-types';

import ProductList from '../../_organisms/ProductsList/products-list';
import Header from '../../_molecules/Header/header';

const CartPage = ({ addProductsInCart, productsInCart }) => (
  <div>
    <Header title="Votre panier" route="/" routeTitle="Produits" />
    <ProductList
      products={productsInCart}
      productsInCart={productsInCart}
      addProductsInCart={addProductsInCart}
    />
  </div>
);

CartPage.propTypes = {
  productsInCart: PropTypes.array,
  addProductsInCart: PropTypes.func,
};

CartPage.defaultProps = {
  productsInCart: null,
  addProductsInCart: () => {},
};

export default CartPage;
