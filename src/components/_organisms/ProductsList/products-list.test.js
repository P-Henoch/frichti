import React from 'react';
import { shallow } from 'enzyme';

import Product from '../../_molecules/Product/product';
import ProductsList from './products-list';

const mockAddProductsInCart = jest.fn();
const mockProduct = { description: 'La soupe aux choux', productId: 1 };
const mockProductsInCart = null;
const mockProducts = [mockProduct];
const wrapper = shallow(
  <ProductsList
    products={mockProducts}
    productsInCart={mockProductsInCart}
    addProductsInCart={mockAddProductsInCart}
  />,
);
const instance = wrapper.instance();

describe('<ProductsList />', () => {
  test('render 0 product', () => {
    const wrapperNoProducts = shallow(
      <ProductsList addProductsInCart={mockAddProductsInCart} />,
    );
    expect(wrapperNoProducts).toMatchSnapshot();
    expect(wrapperNoProducts.find(Product)).toHaveLength(0);
  });
  test('render 1 product', () => {
    const wrapperOneProduct = shallow(
      <ProductsList
        products={mockProducts}
        addProductsInCart={mockAddProductsInCart}
      />,
    );
    expect(wrapperOneProduct).toMatchSnapshot();
    expect(wrapperOneProduct.find(Product)).toHaveLength(1);
  });
  test('add product in cart', () => {
    instance.addProductInCart(mockProduct);
    expect(mockAddProductsInCart).toHaveBeenCalled();
    wrapper.setProps({ productsInCart: [mockProduct] });
    expect(wrapper.instance().props.productsInCart).toEqual([mockProduct]);
  });
  test('remove product from cart', () => {
    instance.removeProductInCart(mockProduct.productId);
    expect(mockAddProductsInCart).toHaveBeenCalled();
    wrapper.setProps({ productsInCart: null });
    expect(wrapper.instance().props.productsInCart).toBeNull();
  });
});
