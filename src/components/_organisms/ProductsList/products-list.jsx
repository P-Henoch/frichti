import React from 'react';
import PropTypes from 'prop-types';

import Product from '../../_molecules/Product/product';

import StyledProductsList from './products-list.style';

class ProductsList extends React.PureComponent {
  addProductInCart = (product) => {
    const { productsInCart, addProductsInCart } = this.props;
    // There is no products in cart,
    // we create the cart add our product and set his quantity to 1
    if (!productsInCart) {
      addProductsInCart([{ ...product, quantity: 1 }]);
      return;
    }
    // Our product is not in the cart,
    // we add the product and set his quantity to 1
    if (
      !productsInCart.some(
        productInCart => productInCart.productId === product.productId,
      )
    ) {
      const updatedProductsInCart = [...productsInCart, { ...product, quantity: 1 }];
      addProductsInCart(updatedProductsInCart);
      return;
    }
    // Our product is in the cart, we increase his quantity by 1
    const updatedProductsInCart = productsInCart.map((productInCart) => {
      if (productInCart.productId !== product.productId) return productInCart;
      return {
        ...product,
        quantity: productInCart.quantity + 1,
      };
    });
    addProductsInCart(updatedProductsInCart);
  }

  removeProductInCart = (productId) => {
    const { productsInCart, addProductsInCart } = this.props;
    const isProductInCart = productsInCart.find(
      productInCart => productInCart.productId === productId,
    );
    // Our product in the cart have a quantity of 1 and there is only 1 product in cart,
    // we remove the cart
    if (isProductInCart.quantity === 1 && productsInCart.length === 1) {
      addProductsInCart(null);
      return;
    }
    // Our product in the cart have a quantity of 1, we remove it from the cart
    if (isProductInCart.quantity === 1) {
      const updatedProductsInCart = productsInCart.filter(
        productInCart => productInCart.productId !== productId,
      );
      addProductsInCart(updatedProductsInCart);
      return;
    }
    // Our product in the cart have a quantity higher than 1, we decrease his quantity by 1
    const updatedProductsInCart = productsInCart.map((productInCart) => {
      if (productInCart.productId !== productId) return productInCart;
      return {
        ...productInCart,
        quantity: productInCart.quantity - 1,
      };
    });
    addProductsInCart(updatedProductsInCart);
  }

  render() {
    const { products, productsInCart } = this.props;
    return (
      <StyledProductsList>
        {products && products.map(product => (
          <Product
            key={product.productId}
            product={product}
            addProductInCart={this.addProductInCart}
            removeProductInCart={this.removeProductInCart}
            productsInCart={productsInCart}
          />
        ))}
      </StyledProductsList>
    );
  }
}

ProductsList.propTypes = {
  products: PropTypes.array,
  productsInCart: PropTypes.array,
  addProductsInCart: PropTypes.func,
};

ProductsList.defaultProps = {
  products: null,
  productsInCart: null,
  addProductsInCart: () => {},
};

export default ProductsList;
