import styled from 'styled-components';

const ProductsList = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 1rem 0;
  justify-content: center;
`;

export default ProductsList;
