import React from 'react';
import { shallow } from 'enzyme';

import StyledHeader from './header.style';

describe('<Header />', () => {
  test('render', () => {
    const wrapper = shallow(
      <StyledHeader />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
