import styled from 'styled-components';

const Header = styled.div`
  display: flex;
  align-items: center;
  padding: 20px;
  flex-wrap: wrap;
`;

Header.link = styled.div`
  margin-left: auto;
`;

export default Header;
