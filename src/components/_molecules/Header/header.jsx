import React from 'react';
import PropTypes from 'prop-types';
import StyledHeader from './header.style';
import Link from '../../_atoms/Link/link';
import Button from '../../_atoms/Button/button';
import Picture from '../../_atoms/Picture/picture';

const Header = ({
  title,
  route,
  routeTitle,
  img,
}) => (
  <StyledHeader>
    <h1>{title}</h1>
    <StyledHeader.link>
      <Button width="60px">
        <Link route={route}>
          <Picture img={img} width="15px" />
          {routeTitle}
        </Link>
      </Button>
    </StyledHeader.link>
  </StyledHeader>
);

Header.propTypes = {
  title: PropTypes.string,
  route: PropTypes.string,
  routeTitle: PropTypes.string,
  img: PropTypes.string,
};

Header.defaultProps = {
  title: '',
  route: '/',
  routeTitle: '',
  img: '',
};

export default Header;
