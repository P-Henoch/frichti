import React from 'react';
import { shallow } from 'enzyme';

import StyledProduct from './product.style';

describe('<Product />', () => {
  test('render', () => {
    const wrapper = shallow(
      <StyledProduct />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
