import styled from 'styled-components';
import COLORS from '../../../constants/style/colors';

const Product = styled.div`
  background-color: white;
  border-radius: 0.4rem;
  box-shadow: 0 2px 4px 0 rgba(139, 159, 196, 0.2);
  width: 200px;
  height: 250px;
  margin: 1rem;
  display: flex;
  flex-direction: column;

  &:hover {
    box-shadow: 0 1.2rem 2.4rem 0 rgba(139, 159, 196, 0.15);
    transition: box-shadow 0.3s ease-in-out, -webkit-box-shadow 0.3s ease-in-out;
  }
`;

Product.productImage = styled.div`
  width: 100%;
  height: 150px;
  background-color: ${COLORS.backgroundLight};
  overflow: hidden;
  flex-shrink: 0;
  display: flex;
  justify-content: center;
`;

Product.productData = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin: 0.6rem;
  height: 100%;
  justify-content: space-between;
`;

Product.productData.group = styled.div`
  display: flex;
  width: 100%;
  align-items: flex-end;
`;

Product.productData.group.info = styled.div`
  display: flex;
`;

Product.productData.group.button = styled.div`
  margin-left: auto;
  display: flex;
`;

export default Product;
