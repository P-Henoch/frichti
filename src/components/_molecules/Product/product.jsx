import React from 'react';
import PropTypes from 'prop-types';
import Price from '../../_atoms/Price/price';
import Title from '../../_atoms/Title/title';
import Picture from '../../_atoms/Picture/picture';
import Quantity from '../../_atoms/Quantity/quantity';
import Button from '../../_atoms/Button/button';
import Plus from '../../../assets/images/plus.svg';
import Minus from '../../../assets/images/minus.svg';

import StyledProduct from './product.style';

const Product = ({
  product,
  addProductInCart,
  removeProductInCart,
  productsInCart,
}) => {
  if (!product || product.price <= 0) return null;
  const isProductInCart = productsInCart && productsInCart.find(
    productInCart => productInCart.productId === product.productId,
  );
  const isProductHaveQuantity = isProductInCart && isProductInCart.quantity;
  const isProductQuantityAtLeastOne = isProductInCart && isProductInCart.quantity >= 1;
  return (
    <StyledProduct>
      <StyledProduct.productImage>
        {product.images[0] && <Picture img={product.images[0]} />}
      </StyledProduct.productImage>
      <StyledProduct.productData>
        <Title title={product.title} />
        <StyledProduct.productData.group>
          <StyledProduct.productData.group.info>
            <Price price={product.price} />
            {isProductHaveQuantity && <Quantity quantity={isProductInCart.quantity} />}
          </StyledProduct.productData.group.info>
          <StyledProduct.productData.group.button>
            {isProductQuantityAtLeastOne && (
              <Button onClick={() => removeProductInCart(product.productId)} type="secondary">
                <Picture img={Minus} width="10px" />
              </Button>
            )}
            <Button onClick={() => addProductInCart(product)}>
              <Picture img={Plus} width="10px" />
            </Button>
          </StyledProduct.productData.group.button>
        </StyledProduct.productData.group>
      </StyledProduct.productData>
    </StyledProduct>
  );
};

Product.propTypes = {
  product: PropTypes.object,
  productsInCart: PropTypes.array,
  addProductInCart: PropTypes.func,
  removeProductInCart: PropTypes.func,
};

Product.defaultProps = {
  product: null,
  productsInCart: null,
  addProductInCart: () => {},
  removeProductInCart: () => {},
};

export default Product;
