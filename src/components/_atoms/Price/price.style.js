import styled from 'styled-components';

const Price = styled.span`
  font-weight: 700;
  font-size: 14px;
  text-align: ${props => props.textAlign},
`;

export default Price;
