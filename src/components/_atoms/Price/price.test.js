import React from 'react';
import { shallow } from 'enzyme';

import StyledPrice from './price.style';

describe('<Price />', () => {
  test('render', () => {
    const wrapper = shallow(
      <StyledPrice />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
