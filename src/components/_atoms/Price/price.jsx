import React from 'react';
import PropTypes from 'prop-types';

import StyledPrice from './price.style';

const Price = ({ price, textAlign }) => {
  const formattedPrice = price / 100;
  return (
    <StyledPrice textAlign={textAlign}>
      {`${formattedPrice}€`}
    </StyledPrice>
  );
};

Price.propTypes = {
  price: PropTypes.number,
  textAlign: PropTypes.string,
};

Price.defaultProps = {
  price: 0,
  textAlign: 'left',
};

export default Price;
