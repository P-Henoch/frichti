import React from 'react';
import PropTypes from 'prop-types';

import StyledPicture from './picture.style';

const Picture = ({ img, width }) => <StyledPicture src={img} width={width} />;

Picture.propTypes = {
  img: PropTypes.string,
  width: PropTypes.string,
};

Picture.defaultProps = {
  img: '',
  width: '100%',
};

export default Picture;
