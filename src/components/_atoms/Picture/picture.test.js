import React from 'react';
import { shallow } from 'enzyme';

import StyledPicture from './picture.style';

describe('<Picture />', () => {
  test('render', () => {
    const wrapper = shallow(
      <StyledPicture />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
