import styled from 'styled-components';

const Picture = styled.img`
  width: ${props => props.width};
`;

export default Picture;
