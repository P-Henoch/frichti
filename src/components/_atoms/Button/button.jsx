import React from 'react';
import PropTypes from 'prop-types';

import StyledButton from './button.style';

const Button = ({
  type,
  onClick,
  children,
  width,
}) => (
  <StyledButton onClick={onClick} type={type} width={width}>
    {children}
  </StyledButton>
);

Button.propTypes = {
  type: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any,
  width: PropTypes.string,
};

Button.defaultProps = {
  type: 'primary',
  onClick: () => {},
  children: '',
  width: '40px',
};

export default Button;
