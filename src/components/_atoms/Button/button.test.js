import React from 'react';
import { shallow } from 'enzyme';

import StyledButton from './button.style';
import Button from './button';

const mockOnClick = jest.fn();

describe('<Button />', () => {
  test('render', () => {
    const wrapper = shallow(<Button onClick={mockOnClick} />);
    expect(wrapper).toMatchSnapshot();
  });
  describe('onClick()', () => {
    test('successfully calls the onClick handler', () => {
      const wrapper = shallow(
        <Button onClick={mockOnClick} />,
      );
      wrapper.find(StyledButton).simulate('click');
      expect(mockOnClick).toHaveBeenCalled();
    });
  });
});
