import styled from 'styled-components';
import COLORS from '../../../constants/style/colors';

const Button = styled.button`
  background-color: ${props => (props.type === 'primary' ? COLORS.primary : COLORS.light)};
  height: 35px;
  width: ${props => props.width};
  border: none;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  outline: none;

  &:hover {
    background-color: ${props => (props.type === 'primary' ? COLORS.primaryHover : COLORS.lightHover)};
  }
`;

export default Button;
