import React from 'react';
import { shallow } from 'enzyme';

import StyledTitle from './title.style';

describe('<Title />', () => {
  test('render', () => {
    const wrapper = shallow(
      <StyledTitle />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
