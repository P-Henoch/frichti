import styled from 'styled-components';

const Title = styled.h2`
  font-weight: 700;
  font-size: 14px;
  margin: 0;
  text-align: ${props => props.textAlign};
`;

export default Title;
