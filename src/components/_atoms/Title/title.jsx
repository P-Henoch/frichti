import React from 'react';
import PropTypes from 'prop-types';

import StyledTitle from './title.style';

const Title = ({ title, textAlign }) => (
  <StyledTitle textAlign={textAlign}>{title}</StyledTitle>
);

Title.propTypes = {
  title: PropTypes.string,
  textAlign: PropTypes.string,
};

Title.defaultProps = {
  title: '',
  textAlign: 'left',
};

export default Title;
