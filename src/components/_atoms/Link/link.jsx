import React from 'react';
import PropTypes from 'prop-types';

import StyledLink from './link.style';

const Link = ({
  route,
  children,
}) => <StyledLink to={route}>{children}</StyledLink>;

Link.propTypes = {
  route: PropTypes.string,
  children: PropTypes.any,
};

Link.defaultProps = {
  route: '/',
  children: '',
};

export default Link;
