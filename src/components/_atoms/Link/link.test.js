import React from 'react';
import { shallow } from 'enzyme';

import StyledLink from './link.style';

describe('<Link />', () => {
  test('render', () => {
    const wrapper = shallow(
      <StyledLink />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
