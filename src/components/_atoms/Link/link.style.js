import styled from 'styled-components';
import { Link as Route } from 'react-router-dom';

const Link = styled(Route)`
  text-decoration: none;
  color: black;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default Link;
