import React from 'react';
import PropTypes from 'prop-types';

import StyledQuantity from './quantity.style';

const Quantity = ({ quantity }) => (
  <StyledQuantity>
    {`x${quantity}`}
  </StyledQuantity>
);

Quantity.propTypes = {
  quantity: PropTypes.number,
};

Quantity.defaultProps = {
  quantity: 0,
};

export default Quantity;
