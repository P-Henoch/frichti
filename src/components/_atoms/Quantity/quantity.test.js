import React from 'react';
import { shallow } from 'enzyme';

import StyledQuantity from './quantity.style';

describe('<Quantity />', () => {
  test('render', () => {
    const wrapper = shallow(
      <StyledQuantity />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
