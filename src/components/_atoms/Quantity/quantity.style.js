import styled from 'styled-components';
import COLORS from '../../../constants/style/colors';

const Quantity = styled.span`
  font-weight: 700;
  font-size: 14px;
  color: ${COLORS.light};
  margin-left: 0.4rem;
`;

export default Quantity;
